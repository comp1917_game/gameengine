#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "Game.h"

#define BOARD_WIDTH 11
#define BOARD_HEIGHT 21
#define DISCIPLINES 0
#define DICEVALUE 1
#define IN_SEA -1

#define NORMAL_EXCHANGE_RATE 3
#define LOWERED_EXCHANGE_RATE 2

#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3
#define NO_DIRECTION 4

#define END_OF_FIRST_COLUMN 3
#define END_OF_SECOND_COLUMN 7
#define END_OF_THIRD_COLUMN 12
#define END_OF_FOURTH_COLUMN 16
#define END_OF_FIFTH_COLUMN 19

typedef struct _university{
   int KPIPoints;
   int numPapers;
   int numPatents;
   int numARCGrants;
   int numGO8s;
   int numCampuses;
   int numStudents[6]; // each array stores number of particular student
    // defined by the #defines
} university;

typedef struct _game {

   int roundNum; // The turn counter , mod by three to obtain player
   university uni[NUM_UNIS]; // array of the players. I noticed in some get
   // functions they use the variable name player so I changed the struct name
   // to university
   int region[NUM_REGIONS][2]; // stores information on the
    // region: discipline and dice numbers
    int board[BOARD_WIDTH][BOARD_HEIGHT]; // stores board as 2D array with alternating
    // campus / ARC locations
} game;

typedef struct _coord {
    int x;
    int y;
} coord;

typedef struct _newStudents {
    int uni1;
    int uni2;
    int uni3;
} newStudents;

// Specify the path the you want to be converted
// It will return the position of the arc and using the orientation
// getCampus should be TRUE or FALSE
static coord pathToCoord(Game g, path thePath, int getCampus);
static newStudents getNewStudents (Game g, int regionID);
static int getBoundedUni(Game g, int x, int y);

//static int isLegalARCPos (Game g, int player, coord arcCoord);
static int getUniIndex(Game g, int player);

static void printBoard(Game g);

Game newGame(int discipline[], int dice[]) {
    Game g = (Game) malloc(sizeof(game));
    assert(g != NULL);
    g->roundNum = -1;
    int i = 0;
    while(i < NUM_REGIONS) {
        g->region[i][DISCIPLINES] = discipline[i];
        g->region[i][DICEVALUE] = dice[i];
        i++;
    }
    i = 0;
    while(i < NUM_UNIS) {
        g->uni[i].KPIPoints = 20;
        g->uni[i].numPapers = 0;
        g->uni[i].numPatents = 0;
        g->uni[i].numARCGrants = 0;
        g->uni[i].numGO8s = 0;
        g->uni[i].numCampuses = 2;
        g->uni[i].numStudents[STUDENT_THD] = 0;
        g->uni[i].numStudents[STUDENT_BPS] = 3;
        g->uni[i].numStudents[STUDENT_BQN] = 3;
        g->uni[i].numStudents[STUDENT_MJ] = 1;
        g->uni[i].numStudents[STUDENT_MTV] = 1;
        g->uni[i].numStudents[STUDENT_MMONEY] = 1;
        i++;
    }

    //printf("working\n");

    i = 0;
    int j = 0;
    while(i < BOARD_WIDTH) {
        int verticalCounter = 3;
        while(j < BOARD_HEIGHT) {
            g->board[i][j] = -1;
            if(i == 0 || i == BOARD_WIDTH - 1) {
                if(j >= 4 && j <= 16 ) {
                    g->board[i][j] = 0;
                }
            }

            if (i == 1 || i == BOARD_WIDTH -2){
                if(j >= 4 && j <= 16 ) {
                    if(verticalCounter == 3) {
                        g->board[i][j] = 0;
                        verticalCounter = 0;
                    } else {
                        verticalCounter ++;
                    }
                }
            }

            if(i == 2 || i == BOARD_WIDTH - 3) {
                if(j >= 2 && j <= 18 ) {
                    g->board[i][j] = 0;
                }
            }

            if (i == 3 || i == BOARD_WIDTH - 4){
                if(j >= 2 && j <= 18 ) {
                    if(verticalCounter == 3) {
                        g->board[i][j] = 0;
                        verticalCounter = 0;
                    } else {
                        verticalCounter ++;
                    }
                }
            }

            if(i == 4 || i == BOARD_WIDTH - 5) {
                g->board[i][j] = 0;
            }

            if (i == 5 || i == BOARD_WIDTH - 6){
                if(verticalCounter == 3) {
                    g->board[i][j] = 0;
                    verticalCounter = 0;
                } else {
                    verticalCounter ++;
                }
            }

            j++;
        }
        j = 0;
        i++;
    }

    g->board[4][0] = CAMPUS_A;
    g->board[6][20] = CAMPUS_A;

    g->board[10][14] = CAMPUS_B;
    g->board[0][6] = CAMPUS_B;

    g->board[10][4] = CAMPUS_C;
    g->board[0][16] = CAMPUS_C;
    printBoard(g);
    return g;

}

void makeAction (Game g, action a){

    assert(a.actionCode!=START_SPINOFF);

    int currentUni = getWhoseTurn(g);
    int currentPlayer= getUniIndex(g, getWhoseTurn(g));
    //actionCode PASS doesn't require makeAction to do anything

    if(a.actionCode==BUILD_CAMPUS){
        //trade in students
        g->uni[currentPlayer].numStudents[STUDENT_BPS]-=1;
        g->uni[currentPlayer].numStudents[STUDENT_BQN]-=1;
        g->uni[currentPlayer].numStudents[STUDENT_MJ]-=1;
        g->uni[currentPlayer].numStudents[STUDENT_MTV]-=1;
        //give KPI points
        g->uni[currentPlayer].KPIPoints+=10;
        //build campus at vertex
        coord campusLocation=pathToCoord(g,a.destination,TRUE);
        g->board[campusLocation.x][campusLocation.y]=getWhoseTurn(g);
        g->uni[currentPlayer].numCampuses++;
        //printf("Building Campus @ (%d,%d) for player: %d\n", campusLocation.x, campusLocation.y, getWhoseTurn(g));


    }else if(a.actionCode==BUILD_GO8){
        //trade in students
        g->uni[currentPlayer].numStudents[STUDENT_MJ]-=2;
        g->uni[currentPlayer].numStudents[STUDENT_MMONEY]-=3;
        //give KPI points
        g->uni[currentPlayer].KPIPoints+=10;
        //build G08 and delete 1 campus
        //since currentPlayer owns that vertex already, just add 3
        coord go8Location=pathToCoord(g,a.destination,TRUE);
        g->board[go8Location.x][go8Location.y]+=3;
        g->uni[currentPlayer].numGO8s++;
        g->uni[currentPlayer].numCampuses--;

        //printf("Building GO8 @ (%d,%d) for player: %d\n", go8Location.x, go8Location.y, getWhoseTurn(g));
    }else if(a.actionCode==OBTAIN_ARC){
        //trade in students
        g->uni[currentPlayer].numStudents[STUDENT_BPS]-=1;
        g->uni[currentPlayer].numStudents[STUDENT_BQN]-=1;
        //give currentPlayer KPI points
        g->uni[currentPlayer].KPIPoints+=2;
        //for prestigem first get current holder/player of most ARCs
        int oldARCPrestige=getMostARCs(g);
        //give currentPlayer an ARC
        coord arcLocation=pathToCoord(g,a.destination,FALSE);

        g->board[arcLocation.x][arcLocation.y]=getWhoseTurn(g);
        g->uni[currentPlayer].numARCGrants++;
        //check if prestige has changed and give/take prestige points
        int newARCPrestige=getMostARCs(g);

        if (newARCPrestige!=oldARCPrestige && newARCPrestige!=NO_ONE){
            g->uni[getUniIndex(g, newARCPrestige)].KPIPoints+=10;
            if(oldARCPrestige != NO_ONE) {
                g->uni[getUniIndex(g, oldARCPrestige)].KPIPoints-=10;
            }
        }
        //printf("Building ARC @ (%d,%d) for player: %d\n", arcLocation.x, arcLocation.y, getWhoseTurn(g));
    }else if(a.actionCode==OBTAIN_PUBLICATION){
        //trade in students
        g->uni[currentPlayer].numStudents[STUDENT_MJ]-=1;
        g->uni[currentPlayer].numStudents[STUDENT_MTV]-=1;
        g->uni[currentPlayer].numStudents[STUDENT_MMONEY]-=1;
        //for prestige, first get current holder/player of most pubs
        int oldPubPrestige=getMostPublications(g);
        //give currentPlayer publicaion
        g->uni[currentPlayer].numPapers++;
        //check if prestige has changed and give/take prestige points
        int newPubPrestige=getMostPublications(g);

        if (newPubPrestige!=oldPubPrestige && newPubPrestige!=NO_ONE){
            g->uni[getUniIndex(g, newPubPrestige)].KPIPoints+=10;
            if(oldPubPrestige != NO_ONE) {
                g->uni[getUniIndex(g, oldPubPrestige)].KPIPoints-=10;
            }
        }

    }else if(a.actionCode==OBTAIN_IP_PATENT){
        //trade in students
        g->uni[currentPlayer].numStudents[STUDENT_MJ]-=1;
        g->uni[currentPlayer].numStudents[STUDENT_MTV]-=1;
        g->uni[currentPlayer].numStudents[STUDENT_MMONEY]-=1;
        //increase numPatent by 1
        g->uni[currentPlayer].numPatents++;
        //give KPI points
        g->uni[currentPlayer].KPIPoints+=10;

    }else if(a.actionCode==RETRAIN_STUDENTS) {
        //get current exchange rate for the specific discipline
        int stuExRate=getExchangeRate(g,currentUni,
                                            a.disciplineFrom,a.disciplineTo);


        //trade in students to be retrained
        g->uni[currentPlayer].numStudents[a.disciplineFrom]-=stuExRate;
        //increase new student
        g->uni[currentPlayer].numStudents[a.disciplineTo]++;

    }
}


//kenji
int getDiscipline (Game g, int regionID){
    return g->region[regionID][DISCIPLINES];
}

//kenji
int getDiceValue (Game g, int regionID){
    return g->region[regionID][DICEVALUE];
}

int getMostARCs(Game g) {
    int i = UNI_A;
    int highest = 0;
    int player = NO_ONE;
    while(i <= NUM_UNIS) {
        if(getARCs(g, i) > highest) {
            highest = getARCs(g, i);
            player = i;
        }
        i++;
    }
    return player;
}
int getMostPublications(Game g) {
    int i = UNI_A;
    int highest = 0;
    int player = NO_ONE;
    while(i <= NUM_UNIS) {
        if(getPublications(g, i) > highest) {
            highest = getPublications(g, getUniIndex(g, i));
        }
        i++;
    }
    return player;
}

int getCampus(Game g, path pathToVertex) {
    coord campus = pathToCoord(g, pathToVertex, TRUE);
    int campusesUni;
    // There is no need to test if the coords are invalid as the
    // isLegalAction should have already caught an invalid path

    campusesUni = g->board[campus.x][campus.y];

    if (campus.x == -1 && campus.y == -1) {
        campusesUni = -1; //off the map
    }

    return campusesUni;
}

int getARC(Game g, path pathToEdge) {
    coord pos = pathToCoord(g, pathToEdge, FALSE);
    int board = VACANT_ARC;
    if(pos.x != -1 && pos.y != -1) {
        board = g->board[pos.x][pos.y];
    }
    return board;
}

int isLegalAction (Game g, action a){

    int isLegal = TRUE;
    int currentUni = getWhoseTurn(g);

    // In the terra nullius phase no action is legal
    if (getTurnNumber(g) == -1) {
        isLegal = FALSE;
    } else if (a.actionCode == PASS) {

        isLegal = TRUE;

    } else if (a.actionCode == BUILD_CAMPUS) {

        //check build_campus

        // Ensure the path is correct
        coord pos = pathToCoord(g, a.destination, TRUE);
        if(pos.x == -1 || pos.y == -1) {
            isLegal = FALSE;
        }

        // 1.check if university has enough students
        if (getStudents(g, currentUni, STUDENT_BPS) < 1 ||
            getStudents(g, currentUni, STUDENT_BQN) < 1 ||
            getStudents(g, currentUni, STUDENT_MJ) < 1 ||
            getStudents(g, currentUni, STUDENT_MTV) < 1)
        {
            isLegal = FALSE;
        }

        // 2.check if the campus is free
        if (getCampus(g,a.destination) != 0){
            isLegal = FALSE;
        }

        // 3.check if the neighbouring campuses are free
        int pathLength = strlen(a.destination);
        char neighbourCampusPath[PATH_LIMIT];
        strcpy(neighbourCampusPath,a.destination);
        int hasNeighbourCampus = FALSE;

        neighbourCampusPath[pathLength+1] = '\0';

        neighbourCampusPath[pathLength] = 'L';
        if (getCampus(g,neighbourCampusPath) != 0 &&
            getCampus(g,neighbourCampusPath) != -1){
            hasNeighbourCampus = TRUE;
        }

        neighbourCampusPath[pathLength] = 'R';

        if (getCampus(g,neighbourCampusPath) != 0 &&
            getCampus(g,neighbourCampusPath) != -1){
            hasNeighbourCampus = TRUE;
        }

        neighbourCampusPath[pathLength] = 'B';
        if (getCampus(g,neighbourCampusPath) != 0 &&
            getCampus(g,neighbourCampusPath) != -1){
            hasNeighbourCampus = TRUE;
        }

        if (hasNeighbourCampus == TRUE){
            isLegal = FALSE;
        }

        // 4.check if the university has an arc connecting to it

        char neighbourArcPath[PATH_LIMIT];
        strcpy(neighbourArcPath,a.destination);
        int hasNeighbourArc = FALSE;
        neighbourArcPath[pathLength+1] = '\0';

        neighbourArcPath[pathLength] = 'L';
        if (getARC(g,neighbourArcPath) == currentUni){
            hasNeighbourArc = TRUE;
        }
        neighbourArcPath[pathLength] = 'R';
        if (getARC(g,neighbourArcPath) == currentUni){
            hasNeighbourArc = TRUE;
        }

        //DEBUGGING
        neighbourArcPath[pathLength] = '\0';
        //neighbourArcPath[pathLength] = 'B';
        if (getARC(g,neighbourArcPath) == currentUni){
            hasNeighbourArc = TRUE;
        }
        if (hasNeighbourArc == FALSE){
            isLegal = FALSE;
        }

        //

    } else if (a.actionCode == BUILD_GO8) {
        //check build_go8

        // Ensure the path is correct
        coord pos = pathToCoord(g, a.destination, TRUE);
        if(pos.x == -1 || pos.y == -1) {
            isLegal = FALSE;
        }

        //1.check if university has enough students
        if (getStudents(g, currentUni, STUDENT_MMONEY) < 3 ||
            getStudents(g, currentUni, STUDENT_MJ) < 2)
        {
            isLegal = FALSE;
        }
        //2.check if the campus is owned by the uni (and not a GO8)
        if (getCampus(g,a.destination) != currentUni){
            isLegal = FALSE;
        }
        //3.check if number of GO8 is less than 8
        if (getGO8s(g,currentUni>=8)){
            isLegal = FALSE;
        }

    } else if (a.actionCode == OBTAIN_ARC) {

        // Check to see if the path is valid
        coord pos = pathToCoord(g, a.destination, FALSE);
        if(pos.x == -1 || pos.y == -1) {
            isLegal = FALSE;
        }

        if (strlen (a.destination) > PATH_LIMIT ||
                    getStudents (g, currentUni, STUDENT_BQN) < 1 ||
                    getStudents (g, currentUni, STUDENT_BPS) < 1 ||
                    g->board[pos.x][pos.y] != VACANT_ARC) {
            isLegal = FALSE;
        }


        //below checks if the university has an arc connecting to it
        //if not, it checks if a campus is conncecting (ie.first turn)
        //added by kenji

        int pathLength = strlen(a.destination);
        char neighbourArcPath[PATH_LIMIT];
        strcpy(neighbourArcPath,a.destination);

        neighbourArcPath[pathLength+1] = '\0';

        int hasNeighbourArc = FALSE;
        neighbourArcPath[pathLength] = 'L';
        if (getARC(g,neighbourArcPath) == currentUni){
            hasNeighbourArc = TRUE;
        }
        neighbourArcPath[pathLength] = 'R';
        if (getARC(g,neighbourArcPath) == currentUni){
            hasNeighbourArc = TRUE;
        }

        neighbourArcPath[pathLength+2] = '\0';

        neighbourArcPath[pathLength] = 'B';
        neighbourArcPath[pathLength+1] = 'L';
        if (getARC(g,neighbourArcPath) == currentUni){
            hasNeighbourArc = TRUE;
        }
        neighbourArcPath[pathLength] = 'B';
        neighbourArcPath[pathLength+1] = 'R';

        if (getARC(g,neighbourArcPath) == currentUni){
            hasNeighbourArc = TRUE;
        }

        //printf("hasNeighbourArc : %d\n",hasNeighbourArc);

        //checks for neighbouring campuses
        int hasNeighbourCampus = FALSE;
        char neighbourCampusPath[PATH_LIMIT];
        strcpy(neighbourCampusPath,a.destination);

        //printf("%d",getCampus(g,neighbourCampusPath));

        neighbourCampusPath[pathLength] = '\0';
        if (getCampus(g,neighbourCampusPath) == currentUni){
            hasNeighbourCampus = TRUE;
        }

        neighbourCampusPath[pathLength+1] = '\0';
        neighbourCampusPath[pathLength] = 'B';
        if (getCampus(g,neighbourCampusPath) == currentUni){
            hasNeighbourCampus = TRUE;
        }

        if (hasNeighbourArc == FALSE && hasNeighbourCampus == FALSE){
            isLegal = FALSE;
        }

        //printf("hasNeighbourCampus : %d\n",hasNeighbourCampus);


    } else if (a.actionCode == START_SPINOFF) {
        if (getStudents (g, currentUni, STUDENT_MTV) < 1 ||
                    getStudents (g, currentUni, STUDENT_MJ) < 1 ||
                    getStudents (g, currentUni, STUDENT_MMONEY) < 1) {
            isLegal = FALSE;
        }
    } else if (a.actionCode == RETRAIN_STUDENTS) {
        if(a.disciplineFrom <= STUDENT_THD || a.disciplineFrom > STUDENT_MMONEY
        || a.disciplineTo <= STUDENT_THD || a.disciplineTo > STUDENT_MMONEY) {
            isLegal = FALSE;
        } else {
            int rate = getExchangeRate (g, currentUni, a.disciplineFrom,
                                                        a.disciplineTo);

            if (getStudents (g, currentUni, a.disciplineFrom) < rate ||
                                        a.disciplineFrom == STUDENT_THD) {
                isLegal = FALSE;
            }
        }
        //printf("Exchange rate: %d, for uni %d, from: %d to: %d while uni has %d\n", rate, currentUni, a.disciplineFrom, a.disciplineTo, getStudents (g, currentUni, a.disciplineFrom));
    } else {
        //any other actions are considered illegal
        isLegal = FALSE;
    }

    if (getTurnNumber(g) == -1){
        isLegal = FALSE;
    }

    return isLegal;
}


/*
static int isLegalARCPos (Game g, int player, coord arcCoord) {
    player = getUniIndex(g,player);
    int isLegal = FALSE;
    int i = -1;
    int j = -1;

    // check for player's arc near its initial campuses
    if (player == UNI_A && ((arcCoord.x == 6 && arcCoord.y == 3) ||
        (arcCoord.x == 7 && arcCoord.y == 2) ||
        (arcCoord.x == 7 && arcCoord.y == 22) ||
        (arcCoord.x == 8 && arcCoord.y == 21))) {
        isLegal = TRUE;
    } else if (player == UNI_B && ((arcCoord.x == 2 && arcCoord.y == 7) ||
        (arcCoord.x == 2 && arcCoord.y == 9) ||
        (arcCoord.x == 12 && arcCoord.y == 15) ||
        (arcCoord.x == 12 && arcCoord.y == 17))) {
        isLegal = TRUE;
    } else if (player == UNI_C && ((arcCoord.x == 2 && arcCoord.y == 17) ||
        (arcCoord.x == 3 && arcCoord.y == 18) ||
        (arcCoord.x == 11 && arcCoord.y == 6) ||
        (arcCoord.x == 12 && arcCoord.y == 7))) {
        isLegal = TRUE;
    }

    // checks the arc is next to another arc of the same player
    if (g->board[arcCoord.x - 1][arcCoord.y - 1] == player ||
        g->board[arcCoord.x + 1][arcCoord.y - 1] == player ||
        g->board[arcCoord.x - 1][arcCoord.y + 1] == player ||
        g->board[arcCoord.x + 1][arcCoord.y + 1] == player) {
        isLegal = TRUE;
    }

    // checks the 4 surrounding arcs to see if an arc will be placed next to
    // another player's arc
    while (j <= 1 && isLegal == TRUE) {
        i = -1;
        while (i <= 1) {
            if (g->board[arcCoord.x + i][arcCoord.y + j] == player % 3 + 1 ||
                g->board[arcCoord.x + i][arcCoord.y + j] == (player + 1) % 3 + 1) {
                isLegal = FALSE;
            }
            i += 2;
        }
        j += 2;
    }

    if (g->board[arcCoord.x][arcCoord.y] != VACANT_ARC) {
        isLegal = FALSE;
    }

    return isLegal;
}
*/


void disposeGame (Game g) {
    free (g);
}

int getTurnNumber (Game g) {
    return g->roundNum;
}

int getWhoseTurn (Game g) {
    int turn;

    if (getTurnNumber (g) == -1) {
        turn = NO_ONE;
    } else {
        turn = (getTurnNumber (g) % NUM_UNIS) + 1;
    }

    return turn;
}

void throwDice (Game g, int diceScore) {
    int i = 0;
    int student;
    newStudents addStudents;
    addStudents.uni1 = 0;
    addStudents.uni2 = 0;
    addStudents.uni3 = 0;
    // check regions and add students
    while (i < NUM_REGIONS) {
        if (g->region[i][DICEVALUE] == diceScore) {
            student = g->region[i][DISCIPLINES];
            addStudents = getNewStudents (g, i);
            // printf("Add students = {%d,%d,%d}\n", addStudents.uni1, addStudents.uni2,addStudents.uni3);
            // printf("The Student is: %d\n", student);
            g->uni[0].numStudents[student] += addStudents.uni1;
            g->uni[1].numStudents[student] += addStudents.uni2;
            g->uni[2].numStudents[student] += addStudents.uni3;
        }
        i++;
    }

    // if roll 7, turn MTVs and MMONEYs into THDs
    if (diceScore == 7) {
        i = 0;
        while (i < NUM_UNIS) {
            g->uni[i].numStudents[STUDENT_THD] +=
                                g->uni[i].numStudents[STUDENT_MTV];
            g->uni[i].numStudents[STUDENT_MTV] = 0;
            g->uni[i].numStudents[STUDENT_THD] +=
                                g->uni[i].numStudents[STUDENT_MMONEY];
            g->uni[i].numStudents[STUDENT_MMONEY] = 0;
            i++;
        }
    }
    //uncomment to print board after every turn
    printBoard(g);

    // increase the round number
    g->roundNum++;

}


// return the number of KPI points the specified player currently has
int getKPIpoints (Game g, int player){
//go into game struct to find the player/uni's KPI points
    int points = 0;
    if(player != NO_ONE) {
        player = getUniIndex(g,player);
        points  = g->uni[player].KPIPoints;
    }

    return points;
}

// return the number of ARC grants the specified player currently has
int getARCs (Game g, int player){
//go into game struct to find the player/uni's ARCs
    int numARCs = 0;
    if(player != NO_ONE) {
        player = getUniIndex(g,player);
        numARCs=g->uni[player].numARCGrants;
    }

    return numARCs;
}

// return the number of GO8 campuses the specified player currently has
int getGO8s (Game g, int player){
//go into game struct to find the player/uni's GO8 Campuses
    int numGO8 = 0;
    if(player != NO_ONE) {
        player = getUniIndex(g,player);
        numGO8=g->uni[player].numGO8s;
    }
    return numGO8;
}

// return the number of normal Campuses the specified player currently has
int getCampuses (Game g, int player){
//go into game struct to find the player/uni's normal Campuses
    int numCampuses = 0;
    if(player != NO_ONE) {
        player = getUniIndex(g,player);
        numCampuses=g->uni[player].numCampuses;
    }
    return numCampuses;
}

// return the number of IP Patents the specified player currently has
int getIPs (Game g, int player){
//go into game struct to find the player/uni's IP Patents
    int numIP = 0;
    if(player != NO_ONE) {
        player = getUniIndex(g,player);
        numIP=g->uni[player].numPatents;
    }
    return numIP;
}

// return the number of Publications the specified player currently has
int getPublications (Game g, int player){
//go into game struct to find the player/uni's Publications
    int numPaper = 0;
    if(player != NO_ONE) {
        player = getUniIndex(g,player);
        numPaper=g->uni[player].numPapers;
    }
    return numPaper;
}

// return the number of students of the specified discipline type
// the specified player currently has
int getStudents (Game g, int player, int discipline){
    int numStudent = 0;
    if(player != NO_ONE) {
        player = getUniIndex(g,player);
        numStudent=g->uni[player].numStudents[discipline];
    }
    return numStudent;
}

// return how many students of discipline type disciplineFrom
// the specified player would need to retrain in order to get one
// student of discipline type disciplineTo.  This will depend
// on what retraining centers, if any, they have a campus at.
int getExchangeRate (Game g, int player,
                     int disciplineFrom, int disciplineTo){
    //player = getUniIndex(g,player);

    assert (player<=3);
    assert(disciplineFrom>=STUDENT_THD);

    int currentExchangeRate=NORMAL_EXCHANGE_RATE;
    //Test to see if player owns campus at the retraining centre for disciplineFrom

    if (disciplineFrom==STUDENT_BPS){
        if (g->board[2][16]==player||g->board[2][18]==player){
            currentExchangeRate=LOWERED_EXCHANGE_RATE;
        }
    }else if(disciplineFrom==STUDENT_BQN){
        if (g->board[10][10]==player||g->board[10][12]==player){
            currentExchangeRate=LOWERED_EXCHANGE_RATE;
        }
    }else if(disciplineFrom==STUDENT_MJ){
        if (g->board[8][16]==player||g->board[8][18]==player){
            currentExchangeRate=LOWERED_EXCHANGE_RATE;
        }
    }else if(disciplineFrom==STUDENT_MTV){
        if (g->board[2][2]==player||g->board[4][2]==player){
            currentExchangeRate=LOWERED_EXCHANGE_RATE;
        }
    }else /*if(disciplineFrom==STUDENT_MMONEY)*/{
        if (g->board[8][4]==player||g->board[8][18]==player){
            currentExchangeRate=LOWERED_EXCHANGE_RATE;
        }
    }

    return currentExchangeRate;
}

static newStudents getNewStudents (Game g, int regionID) {
    newStudents addStudents = {0, 0, 0};
    int i = 0;
    int j = 0;
    int minX = 0;
    int minY = 0;
    // the min x / y are the values on the board that need to be
    // tested for the region

    if (regionID < END_OF_FIRST_COLUMN) {
        minX = 0;
        minY = 4 + 4 * regionID;
    } else if (regionID < END_OF_SECOND_COLUMN) {
        minX = 2;
        minY = 2 + 4 * (regionID - END_OF_FIRST_COLUMN);
    } else if (regionID < END_OF_THIRD_COLUMN) {
        minX = 4;
        minY = 4 * (regionID - END_OF_SECOND_COLUMN);
    }else if (regionID < END_OF_FOURTH_COLUMN) {
        minX = 6;
        minY = 2 + 4 * (regionID - END_OF_THIRD_COLUMN);
    }else if (regionID < END_OF_FIFTH_COLUMN) {
        minX = 8;
        minY = 4 + 4 * (regionID - END_OF_FOURTH_COLUMN);
    }

    j = minY;
    while (j <= minY + 4) {
        i = minX;
        while (i <= minX + 2) {
            if (g->board[i][j] == CAMPUS_A) {
                addStudents.uni1++;
            } else if (g->board[i][j] == CAMPUS_B) {
                addStudents.uni2++;
            } else if (g->board[i][j] == CAMPUS_C) {
                addStudents.uni3++;
            } else if (g->board[i][j] == GO8_A) {
                addStudents.uni1 += 2;
            } else if (g->board[i][j] == GO8_B) {
                addStudents.uni2 += 2;
            } else if (g->board[i][j] == GO8_C) {
                addStudents.uni3 += 2;
            }
            i += 2;
        }
        j += 2;
    }

    return addStudents;
}

/*
 UP 0
 DOWN 1
 LEFT 2
 RIGHT 3
 NO_DIRECTION 4
*/

static coord pathToCoord(Game g, path thePath, int getCampus) {

    // Ensure that getCampus is true of false
    assert(getCampus == TRUE || getCampus == FALSE);
    int i = 0;
    coord pos;
    pos.x = -1;
    pos.y = -1;
    int orientation = NO_DIRECTION;

    int j = 0;
    while(j < strlen(thePath)) {
        if(thePath[j] != 'R' && thePath[j] != 'L' && thePath[j] != 'B' && thePath[j] != '\0') {
            assert(j < PATH_LIMIT);
            //printf("Invalid path \'%s\' with \'%c\'\n", thePath, thePath[j]);
            pos.x = -1;
            pos.y = -1;
            i = strlen(thePath);
        }
        j++;
    }

    if (thePath[0] == 'R') {
        pos.x = 4;
        pos.y = 1;
        orientation = DOWN;
    } else if (thePath[0] == 'L') {
        pos.x = 5;
        pos.y = 0;
        orientation = RIGHT;
    } else if (thePath[0] == 'B') {
        pos.x = -1;
        pos.y = -1;
        i = strlen(thePath);
    } else if(thePath[0] == '\0') {
        pos.x = 4;
        pos.y = 1;
    }
    i++;


    // Check to see if the path as the valid characters

    // printf("I am starting @ Coord = %d,%d f=%d\n", pos.x, pos.y, orientation);

    //printf("Coord %d,%d %d\n", pos.x,pos.y, orientation);

    while(i < strlen(thePath)) {
          //printf("The path[i] = %c\n", thePath[i]);

        if (thePath[i] == 'L') {
            if(orientation == UP) {
                if (getBoundedUni(g, pos.x - 1, pos.y - 1) == -1) {
                    pos.y -= 2;
                } else {
                    pos.y --;
                    pos.x --;
                    orientation = LEFT;
                }
            } else if (orientation == DOWN) {
                if (getBoundedUni(g, pos.x + 1, pos.y + 1) == -1) {
                    pos.y += 2;
                } else {
                    pos.y ++;
                    pos.x ++;
                    orientation = RIGHT;
                }
            } else if (orientation == LEFT) {
                pos.y ++;
                pos.x --;
                orientation = DOWN;
            } else if (orientation == RIGHT) {
                pos.y --;
                pos.x ++;
                orientation = UP;
            }
        }
        if (thePath[i] == 'R') {
            if(orientation == UP) {
                if (getBoundedUni(g, pos.x + 1, pos.y - 1) == -1) {
                    pos.y -= 2;
                } else {
                    pos.y --;
                    pos.x ++;
                    orientation = RIGHT;
                }
            } else if (orientation == DOWN) {
                if (getBoundedUni(g, pos.x - 1, pos.y + 1) == -1) {
                    pos.y += 2;
                } else {
                    pos.y ++;
                    pos.x --;
                    orientation = LEFT;
                }
            } else if (orientation == LEFT) {
                pos.y --;
                pos.x --;
                orientation = UP;
            } else if (orientation == RIGHT) {
                pos.y ++;
                pos.x ++;
                orientation = DOWN;
            }
        }
        if (thePath[i] == 'B') {
            if(orientation == UP) {
                orientation = DOWN;
            } else if (orientation == DOWN) {
                orientation = UP;
            } else if (orientation == LEFT) {
                orientation = RIGHT;
            } else if (orientation == RIGHT) {
                orientation = LEFT;
            }
        }
        if (getBoundedUni(g, pos.x, pos.y) == IN_SEA) {
            //printf("Out of bounds for %s, %d,%d %d Campus:%d\n",thePath, pos.x,pos.y, orientation, getCampus);
            pos.x = -1;
            pos.y = -1;

            i = strlen(thePath);
        }
        //printf("Coord %d,%d %d\n", pos.x,pos.y, orientation);

        i++;
    }

    //printf("Got cood for:C=%d %s = %d,%d\n", getCampus, thePath, pos.x,pos.y);
    if(getCampus == TRUE && pos.x != -1 && pos.y != -1) {
        if(orientation == UP) {
            pos.y --;
        } else if (orientation == DOWN) {
            pos.y ++;
        } else if (orientation == LEFT) {
            pos.x --;
        } else if (orientation == RIGHT) {
            pos.x ++;
        } else if (orientation == NO_DIRECTION) {
            pos.x = 4;
            pos.y = 0;
        }
    }
    // printf("Got cood for:C=%d %s = %d,%d\n\n", getCampus, thePath, pos.x,pos.y);
    return pos;
}
static int getBoundedUni(Game g, int x, int y) {
    if(x >= BOARD_WIDTH || x < 0 || y >= BOARD_HEIGHT || y < 0) {
        return IN_SEA;
    } else {
        return g->board[x][y];
    }
}

static void printBoard(Game g) {
    int y = 0;
    int x = 0;

    while(y < BOARD_HEIGHT) {
        while(x < BOARD_WIDTH) {
            int boardNum = g->board[x][y];
            if (boardNum < 0) {
                printf("   ");
            } else {
                if(x % 2 == 0 && y % 2 == 0) { //We are at a Campus
                    if (boardNum == CAMPUS_A) {
                        printf(" A ");
                    } else if (boardNum == CAMPUS_B) {
                        printf(" B ");
                    } else if (boardNum == CAMPUS_C) {
                        printf(" C ");
                    } else if (boardNum == GO8_A) {
                        printf("GoA");
                    } else if (boardNum == GO8_B) {
                        printf("GoB");
                    } else if (boardNum == GO8_C) {
                        printf("GoC");
                    } else {
                        printf(" : ");
                    }
                } else { //We are at an ARC
                    if (boardNum == CAMPUS_A) {
                        printf(" a ");
                    } else if (boardNum == CAMPUS_B) {
                        printf(" b ");
                    } else if (boardNum == CAMPUS_C) {
                        printf(" c ");
                    } else {
                        printf(" - ");
                    }
                }
            }
            //printf(" ");
            x++;
        }
        y++;
        x = 0;
        printf("\n");
    }
}

static int getUniIndex(Game g, int player) {;
    if(getTurnNumber(g) > -1) {
        if(! (player >= UNI_A && player <= UNI_C)) {
            //printf("Trying to get for player: %d\n",player);
        }
        //assert(player >= UNI_A && player <= UNI_C);
    }
    if(player != NO_ONE) {
        player --;
    }
    return player;
}
