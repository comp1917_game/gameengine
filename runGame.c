#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Game.h"
#include "mechanicalTurk.h"

#include <time.h>
#include <string.h>

//Can switch in different AIs instead of mrPass
//gcc -Wall -std=gnu99 -g -O -fno-omit-frame-pointer -lm -o runGame runGame.c mrPass.c Game.c
//This sample uses AI for uni a and b and human for player c

//If not all code done can at least compile this file
//but not end up with an executable
//gcc -Wall -std=gnu99 -g -O -fno-omit-frame-pointer -c runGame.c

//STILL ROUGH. Students NEED TO FIX FOR runGame.c activity
//Needs to get human move input
//print helpful output
//Make ways to run different boards (ie disciplines,dice permutations)
//Test it is right!! May be buggy? Use at own risk!

/*Tasks
Print helpful output-> status of the game eg KPI, students etc  -->MAdeliene
Human move input -> put in "do you want to make another move?" and scanfs -->Kenji
make ways to run different boards -> maybe use a random num generator for 0-5 disciplines and 2-12 dice values-->ALex
free memory + get AI input --> Mahima
*/

#define WINNING_KPI 150
#define FALSE 0
#define TRUE 1


#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, \
                STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, \
                STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}
#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

int checkForWinner(Game g);
int rollDice(int numRolls, int diceSides);
action getMove(Game g);
action getUserMove(Game g);
void printPlayerStats(Game g, int whoseTurn);


path* getPathInput(void);

int getDisciplineFrom(void);
int getDisciplineTo(void);

int * getBoardDice();
int * getBoardDisciplines();


int main(int argc, char * argv[]){
    srand(time(NULL)); //initialises a seed

    //UNCOMMENT FOR RANDOM BOARD AND DICE
    int* disciplines = getBoardDisciplines();
    int* dice = getBoardDice();


    /*
    //UNCOMMENT FOR ORIGINAL BOARD AND DICE
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    */

    int turnOver = FALSE;
    int winner = NO_ONE;
    int diceValue;
    action move;
    int whoseTurn = NO_ONE;


    Game g = newGame(disciplines,dice);

    while (winner == NO_ONE) {

        diceValue = rollDice(2,6);

        throwDice(g,diceValue);

        printf("[Turn %d] The turn now belongs to University %d!\n\n",
            getTurnNumber(g),
            getWhoseTurn(g));
        printf("The dice casted a %d!\n\n", diceValue);

        whoseTurn = getWhoseTurn(g);
        //loop until player action is PASS
        turnOver = FALSE;
        while (!turnOver) {
            printf("\n");
            printPlayerStats(g,whoseTurn);

            move = getMove(g);
            //print info about move
            //printf("Move code : %d \n",move.actionCode);
            //printf("Move destination : %s \n",move.destination);

            assert(isLegalAction(g, move));

            if (move.actionCode == START_SPINOFF) {
                //decide if outcome is patent or publication
                if (rand() % 3 <= 1) {
                    printf("Publication Obtained!\n\n");
                    move.actionCode = OBTAIN_PUBLICATION;
                } else {
                    printf("IP Patent Obtained!\n\n");
                    move.actionCode = OBTAIN_IP_PATENT;
                }
            }

            if(move.actionCode == PASS){
                 turnOver = TRUE;
                 printf("Pass\n");
            }
            makeAction(g, move);


            if(getKPIpoints(g, whoseTurn) >= WINNING_KPI){
                winner = whoseTurn;
                turnOver = TRUE;
                printf("Winner %d\n",whoseTurn);
            }

        }
        printf("\n");
    }


    // TODO print statistics



    disposeGame (g);
    free(dice);
    free(disciplines);

    return EXIT_SUCCESS;
}


int checkForWinner(Game g) {
   int winner = NO_ONE;
   int playerIndex;

   playerIndex = UNI_A;
   while (playerIndex <= UNI_C) {
      // check if the player is over or equal the WINNING_KPI
      if (getKPIpoints(g, playerIndex) >= WINNING_KPI) {
         winner = playerIndex;
      }
      playerIndex++;
   }

   return winner;
}

int rollDice(int numRolls, int diceSides) {

   int diceRollCount = 0;
   int diceValue = 0;

   //"rolls the die"
   while (diceRollCount < numRolls) {
      diceValue += 1 + rand() % diceSides;
      diceRollCount++;
   }

   return diceValue;
}

//Just one possibility for mixing in AI and human
//Students can modify however they like.
action getMove(Game g){

  action move;

  /*
  //UNCOMMENT TO USE AI
  if(getWhoseTurn(g) == UNI_A || getWhoseTurn(g) == UNI_B){
     move = decideAction(g); //AI
  } else {
      move = getUserMove(g);
  }
  */

  //move = getUserMove(g); //default user input
  move = decideAction(g); //AI input

  /*
  if(getWhoseTurn(g) == UNI_A){
     move = decideAction(g); //AI
  } else {
      move = getUserMove(g);
  }
  */

  printf("Actioncode : %d\n",move.actionCode);

  return move;
}


//gets next move from user
//Leave for them to do
action getUserMove(Game g) {

   action move;
   int userInput;

   printf("Moves are\n\n\
    0 : PASS\n\
    1 : BUILD CAMPUS\n\
    2 : BUILD GO8\n\
    3 : OBTAIN ARC\n\
    4 : START SPINOFF\n\
    5 : RETRAIN STUDENTS\n\n \
Enter move : ");

   while (scanf("%d",&userInput)!=1);

   printf("\n");

   assert(0 <= userInput && userInput <= 7);

   path* inputPath;

   if (userInput == 0){

       move.actionCode = PASS;

   } else if (userInput == 1){

       move.actionCode = BUILD_CAMPUS;
       // input must be a path
       printf("Enter location of Campus as a path : ");

       inputPath = getPathInput();
       strcpy(move.destination, *inputPath);
       free(inputPath);

   } else if (userInput == 2){

       move.actionCode = BUILD_GO8;
       printf("Enter location of GO8 as a path : ");

       inputPath = getPathInput();
       strcpy(move.destination, *inputPath);
       free(inputPath);

   } else if (userInput == 3){

       move.actionCode = OBTAIN_ARC;
       printf("Enter location of ARC Grant as a path : ");

       inputPath = getPathInput();
       strcpy(move.destination, *inputPath);
       free(inputPath);

   } else if (userInput == 4){

       move.actionCode = START_SPINOFF;

   } else if (userInput == 5){

       move.actionCode = RETRAIN_STUDENTS;
       move.disciplineFrom = getDisciplineFrom();
       move.disciplineTo = getDisciplineTo();
   }

    return move;
}


int * getBoardDice() {
    int * r = (int*) malloc(sizeof(int) * 19);
    assert(r != NULL);
    int i = 0;
    while(i < 19) {
        r[i] = (rand()%6) +(rand()%6) ;
        i++;
    }
    return r;
}

int * getBoardDisciplines() {
    int * r = (int*) malloc(sizeof(int) * 19);

    int i = STUDENT_THD;
    int offset = 0;
    while (i <= STUDENT_MMONEY) {
        r[offset] = i;
        r[offset + 1] = i;
        r[offset + 2] = i;
        i++;
        offset +=3;
    }

    // Now we jumble them up
    i = 0;
    while (i < 500) {
        int posOne = rand() % 19;
        int posTwo = rand() % 19;
        int temp = r[posOne];
        r[posOne] = r[posTwo];
        r[posTwo] = temp;
        i++;
    }
    return r;
}
void printPlayerStats(Game g, int whoseTurn){
    printf("Player %d Stats: \n", whoseTurn);
    printf("KPIs         = %d\n", getKPIpoints(g, whoseTurn));
    printf("ARCs         = %d\n", getARCs(g,whoseTurn));
    printf("Campuses     = %d\n", getCampuses(g,whoseTurn));
    printf("GO8s         = %d\n", getGO8s(g,whoseTurn));
    printf("Publications = %d\n", getPublications (g, whoseTurn));
    printf("Patents      = %d\n", getIPs (g, whoseTurn));
    printf("THDs         = %d\n", getStudents(g, whoseTurn, STUDENT_THD));
    printf("BPSs         = %d\n", getStudents(g, whoseTurn, STUDENT_BPS));
    printf("BQNs         = %d\n",  getStudents(g, whoseTurn, STUDENT_BQN));
    printf("MJs          = %d\n", getStudents(g, whoseTurn, STUDENT_MJ));
    printf("MTVs         = %d\n", getStudents(g, whoseTurn, STUDENT_MTV));
    printf("MMONEYs      = %d\n", getStudents(g, whoseTurn, STUDENT_MMONEY));

    printf("\n");
}

path * getPathInput(){

  path * destination = (path*) malloc(sizeof(path));
  assert(destination != NULL);

  //checks scanfs return value. return value = 1 if scanf succeeded.
  //(otherwise program doesn't compile)
  while(scanf("%s",*destination) != 1);

  return destination;
}

int getDisciplineFrom(void){
  int disciplineFrom;

  printf("Disciplines are\n\n\
    0 : THD\n\
    1 : BPS\n\
    2 : BQN\n\
    3 : MJ\n\
    4 : MTV\n\
    5 : MMONEY\n\n\
Enter Discipline from : ");

  while (scanf("%d", &disciplineFrom) != 1);

  printf("\n");

  assert(0 <= disciplineFrom && disciplineFrom <= 5);

  return disciplineFrom;
}

int getDisciplineTo(void){
  int disciplineTo;

  printf("Disciplines are\n\n\
    0 : THD\n\
    1 : BPS\n\
    2 : BQN\n\
    3 : MJ\n\
    4 : MTV\n\
    5 : MMONEY\n\n\
Enter Discipline to : ");

  while (scanf("%d", &disciplineTo) != 1);

  printf("\n");

  assert(0 <= disciplineTo && disciplineTo <= 5);

  return disciplineTo;
}
